<?php
namespace MPXPP;
/**
 * Class implementing MPX MPXPP client interface
 * @author Brian J. Cohen <brian@intercarve.net>
 * @version 3.0
 * @copyright Copyroght (c) 2012 MPX
 *
 */
class Client {

	public $endpoint = 'https://www.mpxpp.com/api';
	public $username = false;
	public $password = false;
	public $application = 'MPXPP Client Application';
	public $sessionId = false;
	public $instance = false;
	public $lastHeaders = array();
	public $payment_gateway_group = 'default';
	public $brand = null;


	/**
	 * Magic factor method that will call loginValidate() and then
	 * the appropriate API call. Eg, if you call $client->streamDocument()
	 * it will get picked up by __call(), which will run loginValidate()
	 * and then api_streamDocument().
	 *
	 * NOTE: This method is NOT designed to be called directly
	 *
	 * @param String $funcName
	 * @param Array $args
	 *
	 * @return Type    Void
	 */
	public function __call($funcName, $args) {
		$this->loginValidate();
		if (method_exists($this, "api_{$funcName}")) {
			return call_user_func_array(array($this, "api_{$funcName}"), $args);
		} else {
			return false;
		}
	}

	/**
	 * Connects to MPXPP with username/pass, gets a sessionId back. You must call this on
	 * every PHP script request at least once, before calling any other methods in this class.
	 * @return	Integer		sessionID
	 * @throws \MPXPP\ClientException;
	 */
	public function loginValidate() {
		if ($this->sessionId == false) {
			list($headers, $body) = $this->query(array(
				"username" => $this->username,
				"password" => $this->password,
				"action" => "loginvalidate",
			));
			$this->lastHeaders = $headers;
			if (intval($headers["MPXPP-Response"]) < 0) {
				throw new ClientException("Error on loginvalidate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
			} else {
				$this->sessionId = $headers["MPXPP-Response"];
				return $this->sessionId;
			}
		}
	}

	public function api_checkIsAlive() {
		list ($headers, $body) = $this->query(array(
			"action" => "checkisalive",
		));
		$this->lastHeaders = $headers;
		if (isset($headers['MPXPP-Response']) && intval($headers["MPXPP-Response"]) < 0) {
			throw new MPXPPClientException("Error on checkisalive, session {$sessionId}: {$headers["MPXPP-Response"]}.");
		} else {
			$response = json_decode($body);
			return $response->status;
		}
	}

	/**
	 * Searches for documents matching supplied criteria
	 *
	 * @param Array $map  Associative array with possible keys: accountnumber (can include a trailing "*" for wildcarding), (date_start AND date_end), sortorder (A/D)
	 * @param Boolean $includeMeta
	 *
	 * @return Type    Description
	 * @throws \MPXPP\ClientException;
	 */
	public function api_searchDocuments($map, $includeMeta = true, $itemsPerPage = 100, $pageNumber = 1, $sortorder = 'A') {
		$params = array(
			"action" => "documentsearch",
			"includemeta" => intval($includeMeta),
			"sortorder" => $sortorder,
			"slice" => $pageNumber,
			"perslice" => $itemsPerPage
		);
		$params = array_merge($params, $map);
		list($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;

		// handle MPXPP error
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on searchdocuments, {$headers["MPXPP-Response"]}.");
			$docs = array();
		} else {
			$docs = json_decode(trim($body));
		}
		return $docs;
	}

	/**
	 * Connects to MPXPP and retrieves a list of documents and their metadata,
	 * given an account number
	 * @param	Integer	$accountnumber
	 * @return	Array	Associative array of docs with properties id,accountid,description,sortkey,is_active,meta. Meta is an associative array as well.
	 * @throws \MPXPP\ClientException;
	 */
	public function api_getDocumentsByAccountId($accountnumber, $includeMeta = true, $sortorder = 'D') {
		list($headers, $body) = $this->query(array(
			"action" => "documentlistbyaccountid",
			"accountnumber" => $accountnumber,
			"includemeta" => intval($includeMeta),
			"sortorder" => $sortorder
		));
		$this->lastHeaders = $headers;
		// handle MPXPP error
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentlistbyaccountid,  {$headers["MPXPP-Response"]}.");
			$docs = array();
		} else {
			$docs = json_decode(trim($body));
		}
		return $docs;
	}

	/**
	 * Connects to MPXPP and retrieves raw document content
	 * @param	Integer	$id	document ID
	 * @return	Binary		Raw PDF data
	 * @throws \MPXPP\ClientException;
	 */
	public function api_fetchDocument($id) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentget",
			"documentid" => $id
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentget,  {$headers["MPXPP-Response"]}.");
		} else {
			return $body;
		}
	}

	/**
	 * Connects to MPXPP and retrieves raw document content
	 * @param	Integer	$id	alternate document ID
	 * @return	Binary		Raw PDF data
	 * @throws \MPXPP\ClientException;
	 */
	public function api_fetchDocumentByAlternateID($id) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentgetbyalternateid",
			"documentalternateid" => $id
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentgetbyalternateid,  {$headers["MPXPP-Response"]}.");
		} else {
			return $body;
		}
	}


	/**
	 * Connects to MPXPP and retrieves raw document content
	 * @param	Mixed	$orderid
	 * @param	Mixed	$lotid
	 * @param	Mixed 	$programid
	 * @param	Mixed	$sequencenumber
	 * @return	Binary		Raw PDF data
	 * @throws \MPXPP\ClientException;
	 */
	public function api_fetchDocumentByMPXSpec($orderid, $lotid, $programid, $sequencenumber) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentgetbympxspec",
			"orderid" => $orderid,
			"lotid" => $lotid,
			"programid" => $programid,
			"sequencenumber" => $sequencenumber
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentgetbympxspec,  {$headers["MPXPP-Response"]}.");
		} else {
			return $body;
		}
	}

	/**
	 * Retrieves the metadata fields available for the current instance
	 *
	 * @return Array
	 * @throws \MPXPP\ClientException;
	 */
	public function api_fetchMetadataFields() {
		list ($headers, $body) = $this->query(array(
			"action" => "metadatafieldsget"
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentget,  {$headers["MPXPP-Response"]}.");
		} else {
			return json_decode(trim($body));
		}
	}

	/**
	 * Retrieves the document object (docstore record) for the specified document
	 *
	 * @param Integer $documentid
	 *
	 * @return Object
	 */
	public function api_getDocumentObject($documentid) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentgetobject",
			"documentid" => $documentid
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentgetobject,  {$headers["MPXPP-Response"]}.");
		} else {
			return json_decode(trim($body));
		}
	}

	/**
	 * Retrieves the document preview for the specified document
	 *
	 * @param Integer $documentid
	 *
	 * @return Object
	 */
	public function api_getDocumentPreview($documentid) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentgetpreview",
			"documentid" => $documentid
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentgetpreview,  {$headers["MPXPP-Response"]}.");
		} else {
			return json_decode(trim($body));
		}
	}

	/**
	 * Retrieves the metadata for the specified document
	 *
	 * @param Integer $documentid
	 *
	 * @return Array
	 */
	public function api_getDocumentMetadata($documentid) {
		list ($headers, $body) = $this->query(array(
			"action" => "documentgetmetadata",
			"documentid" => $documentid
		));
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on documentgetmetadata,  {$headers["MPXPP-Response"]}.");
		} else {
			return json_decode(trim($body));
		}
	}

	/**
	 * Wraps fetchDocument() and writes content to output buffer with download/attachment HTTP headers
	 * @param	Integer	$id	document ID
	 * @return	Void
	 */
	public function api_streamDocument($id, $as_attachment = true) {
		$body = $this->fetchDocument($id);
		if ($body == '') {
			throw new ClientException("Document body was empty.");
		}
		$filename = static::resolveFilename($this->lastHeaders["Content-Disposition"]);

		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: " . $headers["Content-type"]);
		header("Content-Length: " . strlen($body));
		if ($as_attachment) {
			header("Content-Disposition: attachment; filename=\"{$filename}\"");
		} else {
			header("Content-Disposition: inline; filename=\"{$filename}\"");
		}
		echo $body;
		session_write_close();
		exit;

	}

	/**
	 * Wraps fetchDocumentByAlternateID() and writes content to output buffer with download/attachment HTTP headers
	 * @param	Integer	$id	alternate document ID
	 * @return	Void
	 */
	public function api_streamDocumentByAlternateID($id, $as_attachment = true) {
		$body = $this->fetchDocumentByAlternateID($id);
		$filename = static::resolveFilename($this->lastHeaders["Content-Disposition"]);
		if ($body == '') {
			throw new ClientException("Document body was empty.");
		}
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: " . $this->lastHeaders["Content-type"]);
		header("Content-Length: " . strlen($body));
		if ($as_attachment) {
			header("Content-Disposition: attachment; filename=\"{$filename}\"");
		} else {
			header("Content-Disposition: inline; filename=\"{$filename}\"");
		}
		echo $body;
		session_write_close();
		exit;

	}

	protected static function resolveFilename($contentDispositionHeader) {
		$filenameParts0 = explode("; ", $contentDispositionHeader);
		$filenameParts1  = str_replace('"', "", str_replace("filename=\"", "", $filenameParts0[1]));
		$parts = pathinfo($filenameParts1);
		$filename = $parts['filename'] . "_" . time() . "." . $parts['extension'];
		return $filename;
	}

	/**
	 * Sends a notification via MPXPP
	 * @param String $name
	 * @param String $type     HTMLEmail, PlainEmail, SMS
	 * @param String $subject
	 * @param String $message	The message to send. If type is HTMLEmail, this is the HTML message.
	 * @param String $messageAlt If type is HTMLEmail, this is the text part of the message. Blank otherwise, or to let MPXPP auto-convert HTML to text for multipart.
	 * @param Integer $delayMinutes how many minutes in the future to schedule delivery for
	 * @param String $cc	Comma-separated list of recipients for CC
	 * @param String $bcc	Comma-separated list of recipients for BCC
	 * @param Array	 $attachDocIDs	Array of documents (by documentid) to be attached to this notification (limit 5, valid for email only)
	 * @param Array  $attachAltIDs  Array of documents (by alternateid) to be attached to this notification (limit 5, valid for email only)
	 * @param String $orderID 	MPX Order ID
	 * @param String $waitForDocAlternateID 	Don't send until docstore record with this docalternateid is verified to exist
	 *
	 * @return Boolean	True on success
	 * @throws \MPXPP\ClientException;
	 */
	public function api_sendNotification($description, $type, $subject, $message, $messageAlt, $toAddress, $toName, $fromAddress, $fromName, $delayMinutes = 5, $cc = '', $bcc = '', $accountNumber = '', $attachDocIDs = array(), $attachAltIDs = array(), $orderID = '', $waitForDocAlternateID = '') {
		$params = array(
			"action" => "notificationsend",
			"source" => "raw",
			"description" => $description,
			"type" => $type,
			"subject" => $subject,
			"message" => htmlentities($message),
			"messagealt" => $messageAlt,
			"toaddress" => $toAddress,
			"toname" => $toName,
			"delayminutes" => $delayMinutes,
			"cc" => $cc,
			"bcc" => $bcc,
			"accountnumber" => $accountNumber,
			"orderid" => $orderID,
			"waitfordocalternateid" => $waitForDocAlternateID
		);


		if (is_array($attachDocIDs)) {
			foreach ($attachDocIDs as $idx => $docid) {
				$params["attachdocids[{$idx}]"] = $docid;
			}
		}
		if (is_array($attachAltIDs)) {
			foreach ($attachAltIDs as $idx => $docid) {
				$params["attachaltids[{$idx}]"] = $docid;
			}
		}
		list ($headers, $body) = $this->query($params, 'POST');
		$this->lastHeaders = $headers;

		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on notificationsend: {$headers["MPXPP-Response"]} {$headers["MPXPP-Response-Detail"]}");
		} else {
			$retval = json_decode($body);
			return $retval->notificationid;
		}
	}

	/**
	 * Sends a notification via MPXPP, using variable substitution into remotely-stored template
	 * @param String $name
	 * @param String $type     HTMLEmail, PlainEmail, SMS
	 * @param String $subject
	 * @param Array  $variavles  Must be associative array containing map of remote variables to values you want to substitute
	 * @param Integer $delayMinutes how many minutes in the future to schedule delivery for
	 * @param String $cc	Comma-separated list of recipients for CC
	 * @param String $bcc	Comma-separated list of recipients for BCC
	 * @param Array	 $attachDocIDs	Array of documents (by documentid) to be attached to this notification (limit 5, valid for email only)
	 * @param Array  $attachAltIDs  Array of documents (by alternateid) to be attached to this notification (limit 5, valid for email only)
	 * @param String $orderID 	MPX Order ID
	 * @param String $waitForDocAlternateID 	Don't send until docstore record with this docalternateid is verified to exist
	 *
	 * @return Boolean	True on success
	 * @throws \MPXPP\ClientException;
	 */
	public function api_sendNotificationWithVariables($description, $type, $subject, $variables, $toAddress, $toName, $fromAddress, $fromName, $delayMinutes = 5, $cc = '', $bcc = '', $accountNumber = '', $attachDocIDs = array(), $attachAltIDs = array(), $orderID = '', $waitForDocAlternateID = '') {
		$params = array(
			"action" => "notificationsend",
			"source" => "variables",
			"description" => $description,
			"type" => $type,
			"subject" => $subject,
			"message" => '',
			"messagealt" => '',
			"toaddress" => $toAddress,
			"toname" => $toName,
			"delayminutes" => $delayMinutes,
			"cc" => $cc,
			"bcc" => $bcc,
			"accountnumber" => $accountNumber,
			"orderid" => $orderID,
			"waitfordocalternateid" => $waitForDocAlternateID
		);


		if (is_array($attachDocIDs)) {
			foreach ($attachDocIDs as $idx => $docid) {
				$params["attachdocids[{$idx}]"] = $docid;
			}
		}
		if (is_array($attachAltIDs)) {
			foreach ($attachAltIDs as $idx => $docid) {
				$params["attachaltids[{$idx}]"] = $docid;
			}
		}
		if (is_array($variables)) {
			foreach ($variables as $key => $val) {
				$params["variables[{$key}]"] = $val;
			}
		}

		list ($headers, $body) = $this->query($params, 'POST');
		$this->lastHeaders = $headers;

		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on notificationsend: {$headers["MPXPP-Response"]} {$headers["MPXPP-Response-Detail"]}");
		} else {
			$retval = json_decode($body);
			return $retval->notificationid;
		}
	}


	/**
	 * Creates an audit record on the MPXPP server
	 *
	 * @param String $description
	 * @param String $key1
	 * @param String $key2
	 * @param String $key3
	 * @param String $key4
	 *
	 * @return Boolean	True on success
	 * @throws \MPXPP\ClientException;
	 */
	public function api_audit($description, $category = 'Application', $consumerid = null, $userdetail = null) {
		list ($headers, $body) = $this->query(array(
			"action" => "audit",
			"category" => $category,
			"description" => $description,
			"consumerid" => $consumerid,
			"userdetail" => $userdetail,
			"officialclient" => 1
		), 'POST');
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on audit: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			return true;
		}
	}


	/**
	 * Creates a record representing a billing account, to support
	 * Reprint functionality.
	 * NOTE: this is not to be confused with 'consumer' accounts.
	 *
	 * @param String $accountid
	 * @param String $name
	 * @param String $address
	 * @param String $addressAlt
	 * @param String $city
	 * @param String $stateOrProvince
	 * @param String $postalCode
	 * @param String $country
	 *
	 * @return Boolean
	 */
	public function api_createAccount($accountid, $name = null, $address = null, $addressAlt = null, $city = null, $state = null, $zip = null, $country = null) {

		list ($headers, $body) = $this->query(array(
			"action" => "accountcreate",
			"accountid" => $accountid,
			"name" => $name,
			"address" => $address,
			"addressalt" => $addressAlt,
			"city" => $city,
			"state" => $state,
			"zip" => $zip,
			"country" => $country
		));
		$this->lastHeaders = $headers;

		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on accountcreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			return true;
		}

	}

	/**
	 * Creates a consumer record on the MPXPP server
	 *
	 * @param String $firstname   optional
	 * @param String $middlename  optional
	 * @param String $lastname    optional
	 * @param String $username
	 * @param String $password
	 * @param String $email1
	 * @param String $email2      optional
	 * @param String $metadata    PHP-style array
	 * @param Boolean $passwordistemp optional
	 *
	 * @return String    ID of the newly-created consumer
	 */
	public function api_createConsumer($firstname, $middlename, $lastname, $username, $password, $email1, $email2, $metadata = array(), $passwordistemp = 0, $passwordhint = null) {
		$params = array(
			"action" => "consumercreate",
			"firstname" => $firstname,
			"middlename" => $middlename,
			"lastname" => $lastname,
			"username" => $username,
			"password" => $password,
			"email1" => $email1,
			"email2" => $email2,
			"passwordistemp" => $passwordistemp
		);
		if (is_array($metadata)) {
			foreach ($metadata as $key => $val) {
				$params["metadata[{$key}]"] = $val;
			}
		}
		if (! is_null($passwordhint)) {
			$params["passwordhint"] = $passwordhint;
		}

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumercreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->consumerid;
		}
	}

	/**
	 * Update an existing consumer record on the MPXPP server
	 *
	 * @param String $consumerid  The ID of the consumer you want to update
	 * @param String $firstname   optional
	 * @param String $middlename  optional
	 * @param String $lastname    optional
	 * @param String $username    optional
	 * @param String $password    optional
	 * @param String $email1      optional
	 * @param String $email2      optional
	 * @param String $metadata    PHP-style array
	 * @param Boolean $passwordistemp  optional
	 * @param Boolean $isautopay	optional
	 * @param String  $autopayvaultid optional
	 * @return String    ID of the updated consumer
	 */
	public function api_updateConsumer($consumerid, $firstname = null, $middlename = null, $lastname = null, $username = null, $password = null, $email1 = null, $email2 = null, $metadata = array(), $passwordistemp = null, $passwordhint = null, $islockedout = null) {
		$params = array(
			"action" => "consumerupdate",
			"consumerid" => $consumerid
		);

		$params_supp = array();

		foreach (array("firstname", "middlename", "lastname", "username", "password", "email1", "email2", "passwordistemp", "islockedout") as $field) {
			if (! is_null($$field) ) {
				$params_supp[$field] = $$field;
			}
		}

		if (is_array($metadata)) {
			foreach ($metadata as $key => $val) {
				$params["metadata[{$key}]"] = $val;
			}
		}

		$params = array_merge($params, $params_supp);
		if (! is_null($passwordhint)) {
			$params["passwordhint"] = $passwordhint;
		}
		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumerupdate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->consumerid;
		}
	}

	/**
	 * Setup an email confirmation loop
	 *
	 * @param String $consumerid  The ID of the consumer
	 * @param String $email The email you want to switch to
	 * @param Integer $which 1 or 2
	 *
	 * @return String    ID of the updated consumer
	 */
	public function api_setupConsumerEmailConfirmation($consumerid, $email, $which = 1, $send_email = 1, $url = null) {
		$params = array(
			"action" => "consumersetupemailconfirmation",
			"consumerid" => $consumerid,
			"email" => $email,
			"which" => $which,
			"send_email" => $send_email,
			"url" => $url
		);


		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumersetupemailconfirmation: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Complete an email confirmation loop
	 *
	 * @param String $consumerid  The ID of the consumer
	 * @param Integer $which 1 or 2
	 *
	 * @return String    ID of the updated consumer
	 */
	public function api_completeConsumerEmailConfirmation($consumerid, $code, $which = 1) {
		$params = array(
			"action" => "consumercompleteemailconfirmation",
			"consumerid" => $consumerid,
			"code" => $code,
			"which" => $which
		);


		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumercompleteemailconfirmation: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}


	/**
	 * Fetch a consumer record from the MPXPP server
	 *
	 * @param String $consumerid
	 *
	 * @return Object
	 */
	public function api_getConsumer($consumerid) {
		$params = array(
			"action" => "consumerget",
			"consumerid" => $consumerid
		);

		if (!empty($this->brand)) {
			$params["brand"] = $this->brand;
		}

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumerget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Fetch a consumer record from the MPXPP server
	 *
	 * @param String $username
	 *
	 * @return Object
	 */
	public function api_getConsumerByUsername($username) {
		$params = array(
			"action" => "consumerget",
			"username" => $username
		);

		if (!empty($this->brand)) {
			$params["brand"] = $this->brand;
		}

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumerget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Authenticate a consumer by username/password
	 *
	 * @param String $username
	 * @param Password $password
	 *
	 * @return Boolean
	 */
	public function api_authConsumer($username, $password, $otc = null) {
 		$params = array(
 			"action" => "consumerauth",
 			"username" => $username,
 			"password" => $password,
 			"otc" => $otc
 		);

		if (!empty($this->brand)) {
			$params["brand"] = $this->brand;
		}

 		list ($headers, $body) = $this->query($params);

 		$this->lastHeaders = $headers;
 		if (intval($headers["MPXPP-Response"]) < 0) {
 			throw new ClientException("Error on consumerauth: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
 		} else {
 			$response = json_decode(trim($body));
 			return $response;
 		}
 	}
	/**
	 * Search for consumer records
	 *
	 * @param Array $pairs Associative array with keys for any of firstname, middlename, lastname, username, email1, email2. You may also include any valid metadata column for this instance.
	 * @param Integer $itemsPerPage  How many items to return
	 * @param Integer $pageNumber Given $itemsPerPage items per page, which page should we return?
	 *
	 * @return Array    Array contains pagination details and a key 'results' which is an array of objects of results
	 */
	public function api_searchConsumers($map = array(), $itemsPerPage = 100, $pageNumber = 1) {
		$params = array(
			"action" => "consumersearch",
			"slice" => $pageNumber,
			"perslice" => $itemsPerPage
		);

		$params = array_merge($params, $map);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumersearch: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Creates a consumer record on the MPXPP server
	 *
	 * @param String $alternateid  Client-supplied ID of account
	 *
	 * @return String    ID of the newly-created consumer account
	 */
	public function api_createConsumerAccount($alternateid, $isautopay, $ispaperless = 0) {
		$params = array(
			"action" => "consumeraccountcreate",
			"alternateid" => $alternateid,
			"ispaperless" => $ispaperless
		);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountcreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->accountid;
		}
	}

	/**
	 * Update an existing consumer record on the MPXPP server
	 *
	 * @param String $accountid  MPXPP numeric or pubid
	 * @return String    ID of the updated consumer account
	 */
	public function api_updateConsumerAccount($accountid, $isautopay = null, $autopayvaultid = null, $ispaperless = null, $autopayoptions = null, $istext2pay = null, $text2payvaultid = null, $text2payoptions = null) {
		$params = array(
			"action" => "consumeraccountupdate",
			"accountid" => $accountid,
		);

		if (! is_null($isautopay)) {
			$params["isautopay"] = $isautopay;
			$params["autopayvaultid"] = $autopayvaultid;
			
			if ($params["isautopay"]) {
				if (! is_null($autopayoptions)) {
					$params["autopayoptions"] = $autopayoptions;
				}
			}
		}
		if (! is_null($istext2pay) && ! is_null($text2payvaultid)) {
			$params['istext2pay'] = $istext2pay;
			$params['text2payvaultid'] = $text2payvaultid;
			if ($params["istext2pay"]) {
				if (! is_null($text2payoptions)) {
					$params["text2payoptions"] = $text2payoptions;
				}
			}

		}
		if (! is_null($ispaperless)) {
			$params["ispaperless"] = $ispaperless;
		}

		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountupdate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->accountid;
		}
	}

	/**
	 * Link a consumer account to a consumer
	 *
	 * @param String $accountid  MPXPP numeric or pubid
	 * @param String $consumerid MPXPP numeric or pubid
	 * @return String    ID of the updated consumer account
	 */
	public function api_linkConsumerAccountToConsumer($accountid, $consumerid, $nickname = '') {
		$params = array(
			"action" => "consumeraccountlinkconsumer",
			"accountid" => $accountid,
			"consumerid" => $consumerid,
			"nickname" => $nickname,
		);

		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountlinkconsumer: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->accountid;
		}
	}

	/**
	 * Unlink a conumser account from a consumer
	 *
	 * @param String $accountid  MPXPP numeric or pubid
	 * @param String $consumerid MPXPP numeric or pubid
	 * @return String    ID of the updated consumer account
	 */
	public function api_unlinkConsumerAccountFromConsumer($accountid, $consumerid) {
		$params = array(
			"action" => "consumeraccountunlinkconsumer",
			"accountid" => $accountid,
			"consumerid" => $consumerid
		);
		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountunlinkconsumer: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->accountid;
		}
	}

	/**
	 * Fetch a consumer account record from the MPXPP server
	 *
	 * @param String $accountid  Can be client-supplied alternate id or MPXPP numeric or pubid
	 *
	 * @return Object
	 */
	public function api_getConsumerAccount($accountid) {
		$params = array(
			"action" => "consumeraccountget",
			"accountid" => $accountid
		);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Search for consumer account records from the MPXPP server
	 *
	 * @param String $alternateid
	 *
	 * @return Object
	 */
	public function api_searchConsumerAccounts($map = array()) {
		$params = array(
			"action" => "consumeraccountsearch",
		);
		$params = array_merge($params, $map);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on consumeraccountsearch: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Charge credit card or bank account without linking it to a consumer.
	 *
	 * @param Decimal $amount
	 * @param String $type               CC, ACH, or Vault
	 * @param Array $billingParams       Keys are billing_name, billing_address, billing_addressalt, billing_city, billing_state, billing_postal, billing_country
	 * @param Array $accountParams     If type=CC, keys are ccnum, ccexp_month, ccexp_year, cccvv. If type=ACH, keys are account, routing, account_type, segment (business/personal). If type=Vault, key is vaultid
	 * @param Integer $delayhours        How many hours into the future we should wait before attempting to run this transaction
	 * @param String $consumeraccountalternateid
	 * @param String $note
	 *
	 * @return Object    Object has two properties: result (boolean) and paymentid (if successful)
	 */
	public function api_createPaymentAsGuest($amount, $type, $billingParams = array(), $accountParams = array(), $delayhours = 0, $consumeraccountalternateid = null, $note = null) {
		$type = strtoupper($type);

		$params = array(
			"action" => "paymentcreateasguest",
			"amount" => $amount,
			"type" => $type,
			"delayhours" => $delayhours,
			"consumeraccountalternateid" => $consumeraccountalternateid,
			"note" => $note,
			"gateway_group" => $this->payment_gateway_group
		);
		foreach ($billingParams as $field => $value) {
			$params[$field] = $value;
		}

		if ($type == 'CC') {
			foreach (array("ccnum", "ccexp_month", "ccexp_year", "cccvv") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'ACH') {
			foreach (array("account", "routing", "account_type", "segment") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'VAULT') {
			if (isset($accountParams["vaultid"])) {
				$params["vaultid"] = $accountParams["vaultid"];
			}
		}
		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentcreateasguest: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Fetch a payment record from the MPXPP server
	 *
	 * @param String $paymentid
	 *
	 * @return Object
	 */
	public function api_getPayment($paymentid) {
		$params = array(
			"action" => "paymentget",
			"paymentid" => $paymentid
		);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Charge a consumer's credit card or bank account -- internal method. Recommended to use
	 * createPaymentByConsumerID() or createPaymentByConsumerAlternateID() instead.
	 *
	 * @param String $consumerid
	 * @param Decimal $amount
	 * @param String $type               CC, ACH, or Vault
	 * @param Array $billingParams       Keys are billing_name, billing_address, billing_addressalt, billing_city, billing_state, billing_postal, billing_country
	 * @param Array $accountParams     If type=CC, keys are ccnum, ccexp_month, ccexp_year, cccvv. If type=ACH, keys are account, routing, account_type, segment (business/personal). If type=Vault, key is vaultid
	 * @param Integer $delayhours        How many hours into the future we should wait before attempting to run this transaction
	 * @param String $consumeraccountalternateid
	 * @param String $note
	 *
	 * @return Object    Object has two properties: result (boolean) and paymentid (if successful)
	 */
	public function api_createPayment($consumerid, $amount, $type, $billingParams = array(), $accountParams = array(), $delayhours = 0, $consumeraccountalternateid = null, $note = null) {
		$type = strtoupper($type);

		$params = array(
			"action" => "paymentcreate",
			"consumerid" => $consumerid,
			"amount" => $amount,
			"type" => $type,
			"delayhours" => $delayhours,
			"consumeraccountalternateid" => $consumeraccountalternateid,
			"note" => $note,
			"gateway_group" => $this->payment_gateway_group
		);
		foreach ($billingParams as $field => $value) {
			$params[$field] = $value;
		}

		if ($type == 'CC') {
			foreach (array("ccnum", "ccexp_month", "ccexp_year", "cccvv") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'ACH') {
			foreach (array("account", "routing", "account_type", "segment") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'VAULT') {
			if (isset($accountParams["vaultid"])) {
				$params["vaultid"] = $accountParams["vaultid"];
			}
		}
		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentcreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Cancels a payment
	 *
	 * @param String $paymentid      paymentpubid or numerical paymentid
	 *
	 * @return Object     Object has two properties: result (boolean) and paymentid (if successful)
	 */
	public function api_cancelPayment($paymentid) {

		$params = array(
			"action" => "paymentcancel",
			"paymentid" => $paymentid
		);


		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentcancel: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Creates a recurring payment subscription
	 *
	 * @param String $consumerid      consumerpubid or numerical consumerid
	 * @param Decimal $amount
	 * @param String $vaultid         vaultpubid or numerical vaultid
	 * @param String $interval        day, month, or year
	 * @param Integer $interval_count  eg. 30
	 * @param Integer $timestamp_start unix timestamp
	 * @param Integer $timestamp_stop	UNIX timestamp or null
	 * @param Integer $max_count		Number of iterations after which to stop the subscription (or null)
	 * @param Integer $lead_hours      number of hours (or zero) in advance of next charge datetime that MPXPP will create the scheduled payment
	 * @param Integer $consumeraccountalternateid
	 * @param Integer $strategy      'natural' for standard strategy, 'monthly' for selecting a specific day of the month
	 * @param Integer $day_of_month      1 - 28 or 'last'
	 *
	 * @return Object     Object has two properties: result (boolean) and subscriptionid (if successful)
	 */
	public function api_createPaymentSubscription($consumerid, $amount, $vaultid, $interval, $interval_count, $timestamp_start, $timestamp_stop = null, $max_count = null, $lead_hours = 4, $consumeraccountalternateid = null, $strategy = 'natural', $day_of_month = null) {

		$params = array(
			"action" => "paymentsubscriptioncreate",
			"consumerid" => $consumerid,
			"amount" => $amount,
			"vaultid" => $vaultid,
			"strategy" => $strategy,
			"interval" => $interval,
			"interval_count" => $interval_count,
			"day_of_month" => $day_of_month,
			"timestamp_start" => $timestamp_start,
			"timestamp_stop" => $timestamp_stop,
			"max_count" => $max_count,
			"lead_hours" => $lead_hours,
			"consumeraccountalternateid" => $consumeraccountalternateid
		);
		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptioncreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Simulates creation or updating of a payment subscription, returns what the subscription would look like, along with a schedule
	 *
	 * @param String $consumerid      consumerpubid or numerical consumerid
	 * @param String $subscriptionid	subscriptionid (optional - only to simulate updating)
	 * @param Decimal $amount
	 * @param String $vaultid         vaultpubid or numerical vaultid
	 * @param String $interval        day, month, or year
	 * @param Integer $interval_count  eg. 30
	 * @param Integer $timestamp_start unix timestamp
	 * @param Integer $timestamp_stop	UNIX timestamp or null
	 * @param Integer $max_count		Number of iterations after which to stop the subscription (or null)
	 * @param Integer $lead_hours      number of hours (or zero) in advance of next charge datetime that MPXPP will create the scheduled payment
	 * @param Integer $strategy      'natural' for standard strategy, 'monthly' for selecting a specific day of the month
	 * @param Integer $day_of_month      1 - 28 or 'last'
	 *
	 * @return Object     Object has two properties: result (boolean) and subscriptionid (if successful)
	 */
	public function api_proposePaymentSubscription($consumerid, $subscriptionid, $amount, $vaultid, $interval, $interval_count, $timestamp_start, $timestamp_stop = null, $max_count = null, $lead_hours = 4, $consumeraccountalternateid = null, $strategy = 'natural', $day_of_month = null) {

		$params = array(
			"action" => "paymentsubscriptionpropose",
			"subscriptionid" => $subscriptionid,
			"consumerid" => $consumerid,
			"amount" => $amount,
			"vaultid" => $vaultid,
			"strategy" => $strategy,
			"interval" => $interval,
			"interval_count" => $interval_count,
			"day_of_month" => $day_of_month,
			"timestamp_start" => $timestamp_start,
			"timestamp_stop" => $timestamp_stop,
			"max_count" => $max_count,
			"lead_hours" => $lead_hours,
			"consumeraccountalternateid" => $consumeraccountalternateid
		);
		list ($headers, $body) = $this->query($params);
		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptionpropose: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}


	/**
	 * Updates a recurring payment subscription
	 *
	 * @param String $subscriptionid      subscriptionpubid or numerical subscriptionid
	 * @param Decimal $amount
	 * @param String $vaultid         vaultpubid or numerical vaultid
	 * @param Integer $timestamp_stop	UNIX timestamp or null
	 * @param Integer $max_count		Number of iterations after which to stop the subscription (or null)
	 * @param Integer $lead_hours      number of hours (or zero) in advance of next charge datetime that MPXPP will create the scheduled payment
	 *
	 * @return Object     Object has two properties: result (boolean) and subscriptionid (if successful)
	 */
	public function api_updatePaymentSubscription($subscriptionid, $amount, $vaultid, $timestamp_stop, $max_count, $lead_hours) {

		$params = array(
			"action" => "paymentsubscriptionupdate",
			"subscriptionid" => $subscriptionid
		);

		$params['amount'] = $amount;
		$params['vaultid'] = $vaultid;
		$params['lead_hours'] = $lead_hours;
		$params['timestamp_stop'] = $timestamp_stop;
		$params['max_count'] = $max_count;
		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptionupdate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Cancels a recurring payment subscription
	 *
	 * @param String $subscriptionid      subscriptionpubid or numerical subscriptionid
	 *
	 * @return Object     Object has two properties: result (boolean) and subscriptionid (if successful)
	 */
	public function api_cancelPaymentSubscription($subscriptionid) {

		$params = array(
			"action" => "paymentsubscriptioncancel",
			"subscriptionid" => $subscriptionid
		);


		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptioncancel: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Lists all active (non-cancelled) recurring payment subscriptions for a consumer
	 *
	 * @param String $consumerid consumerpubid or numerical consumerid
	 *
	 * @return Object    Object has two properties: status (boolean) and results (array of objects returned by search)
	 */
	public function api_listPaymentSubscriptions($consumerid) {

		$params = array(
			"action" => "paymentsubscriptionlist",
			"consumerid" => $consumerid
		);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptionlist: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Fetches a single recurring payment subscription for a consumer
	 *
	 * @param String $subscriptionid subscriptionpubid or numerical subscriptionid
	 *
	 * @return Object    Object has two properties: status (boolean) and result (objects returned by search)
	 */
	public function api_getPaymentSubscription($subscriptionid) {

		$params = array(
			"action" => "paymentsubscriptionget",
			"subscriptionid" => $subscriptionid,
		);


		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptionget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Fetches a single recurring payment subscription for a consumer
	 *
	 * @param String $subscriptionid subscriptionpubid or numerical subscriptionid
	 *
	 * @return Object    Object has two properties: status (boolean) and result (objects returned by search)
	 */
	public function api_getPaymentSubscriptionSchedule($subscriptionid) {

		$params = array(
			"action" => "paymentsubscriptionscheduleget",
			"subscriptionid" => $subscriptionid,
		);


		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentsubscriptionscheduleget: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}


	/**
	 * Create a stored payment method on the MPXPP server
	 *
	 * @param String $consumerid
	 * @param String $type                Either CC or ACH
	 * @param Array $billingParams       Keys are billing_name, billing_address, billing_addressalt, billing_city, billing_state, billing_postal, billing_country
	 * @param Array $accountParams       If type=CC, keys are ccnum, ccexp_month, ccexp_year, cccvv. If type=ACH, keys are account, routing, account_type, segment (business/personal).
	 *
	 * @return Object    Properties of object are 'result' (boolean) and 'vaultid' (if result true)
	 */
	public function api_createPaymentVaultRecord($consumerid, $type, $billingParams = array(), $accountParams = array(), $nickname = null) {
		$type = strtoupper($type);
		$params = array(
			"action" => "paymentvaultcreate",
			"consumerid" => $consumerid,
			"type" => $type,
			"gateway_group" => $this->payment_gateway_group
		);
		foreach ($billingParams as $field => $value) {
			$params[$field] = $value;
		}

		if ($type == 'CC') {
			foreach (array("ccnum", "ccexp_month", "ccexp_year", "cccvv") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'ACH') {
			foreach (array("account", "routing", "account_type", "segment") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		}
		if (! is_null($nickname) ) {
			$params['nickname'] = $nickname;
		}

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentvaultcreate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}

	/**
	 * Update an existing stored payment method on the MPXPP server
	 *
	 * @param String $consumerid
	 * @param String $vaultid
	 * @param String $type                Either CC or ACH
	 * @param Array $billingParams       Keys are billing_name, billing_address, billing_addressalt, billing_city, billing_state, billing_postal, billing_country
	 * @param Array $accountParams       If type=CC, keys are ccnum, ccexp_month, ccexp_year, cccvv. If type=ACH, keys are account, routing, account_type, segment (business/personal).
	 *
	 * @return Object    Properties of object are 'result' (boolean) and 'vaultid' (if result true)
	 */
	public function api_updatePaymentVaultRecord($vaultid, $consumerid, $type, $billingParams = array(), $accountParams = array(), $nickname = null) {
		$type = strtoupper($type);
		$params = array(
			"action" => "paymentvaultupdate",
			"vaultid" => $vaultid,
			"consumerid" => $consumerid,
			"type" => $type,
		);
		foreach ($billingParams as $field => $value) {
			$params[$field] = $value;
		}

		if ($type == 'CC') {
			foreach (array("ccnum", "ccexp_month", "ccexp_year", "cccvv") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		} elseif ($type == 'ACH') {
			foreach (array("account", "routing", "account_type", "segment") as $field) {
				if (isset($accountParams[$field])) {
					$params[$field] = $accountParams[$field];
				}
			}
		}
		if (! is_null($nickname) ) {
			$params['nickname'] = $nickname;
		}
		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentvaultupdate: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response;
		}
	}


	/**
	 * Delete an existing stored payment method on the MPXPP server
	 *
	 * @param String $vaultid
	 * @param String $consumerid
	 *
	 * @return Boolean
	 */
	public function api_deletePaymentVaultRecord($vaultid) {

		$params = array(
			"action" => "paymentvaultdelete",
			"vaultid" => $vaultid,
		);


		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentvaultdelete: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->result;
		}
	}

	/**
	 * Retrieve a list of stored payment methods for a consumer
	 *
	 * @param String $consumerid
	 *
	 * @return Object  property 'results' contains an array of result objects
	 */
	public function api_listPaymentVaultRecords($consumerid) {
		$params = array(
			"action" => "paymentvaultlist",
			"consumerid" => $consumerid,
		);

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentvaultlist: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->results;
		}
	}

	/**
	 * Retrieve a list of payments made by a consumer
	 *
	 * @param String $consumerid
	 * @param Array $statusMap  A list of Status values to filter to. Leave empty to return for all statuses.
	 * @param Boolean $remote  If true, will query the remote gateway (and/or mpxpp cache of those queries). Default is false (queries MPXPP).
	 * @return Object  property 'results' contains an array of result objects
	 */
	public function api_listPayments($consumeraccountalternateid, $statusMap = array(), $remote = false) {
		$params = array(
			"action" => "paymentlist",
			"consumeraccountalternateid" => $consumeraccountalternateid,
			"remote" => $remote
		);
		foreach ($statusMap as $idx => $status) {
			$params["status[{$idx}]"] = $status;
		}

		list ($headers, $body) = $this->query($params);

		$this->lastHeaders = $headers;
		if (intval($headers["MPXPP-Response"]) < 0) {
			throw new ClientException("Error on paymentlist: {$headers["MPXPP-Response"]}, {$headers["MPXPP-Response-Detail"]}.");
		} else {
			$response = json_decode(trim($body));
			return $response->results;
		}
	}

	/**
	 * Retrieves the server's currently-running API environment (dev, test, prd)
	 *
	 * @return Array
	 */
	public function api_env() {
		list ($headers, $body) = $this->query(array(
			"action" => "env"
		));
		$this->lastHeaders = $headers;
		return json_decode($body);
	}

	/**
	 * Retrieves the current instance's vital statistics
	 *
	 * @return Array
	 */
	public function stats() {
		list ($headers, $body) = $this->query(array(
			"action" => "stats"
		));
		$this->lastHeaders = $headers;
		return json_decode($body);
	}

	/**
	 * Retrieves the current instance's EDAPT configuration, if present
	 *
	 * @return Object
	 */
	public function api_getEDAPTConfig($brand = 'default') {
		list ($headers, $body) = $this->query(array(
			"action" => "edaptconfig",
			"brand" => $brand
		));
		return json_decode($body);
	}

	/**
	 * Makes a direct query to the remote MPXPP system
	 * @param	Array	$params	parameters, per MPXPP documentation
	 * @return	Array			($headers, $body)
	 */
	public function query($params, $method = 'POST') {
		$params["clientid"] = "MPXPP_Client/PHP";
		$params["clientdetail"] = "PHP " . phpversion();
		$params["application"] = $this->application;
		$params["instance"] = $this->instance;
		if ($this->sessionId !== false) {
			$params["sessionid"] = $this->sessionId;
		}

		if ($method == 'GET') {
			$url = str_replace('?', '', $endpoint) . "?" . http_build_query($params);
		} else {
			$url = $this->endpoint;
		}


		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 60 * 3);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);

		if ($method == 'POST') {
			curl_setopt($curl, CURLOPT_POST, true);
			$_params = array();
			foreach ($params as $k1 => $v1) {
				if (is_array($v1)) {
					foreach ($v1 as $k2 => $v2) {
						if (is_array($v2)) {
							foreach ($v2 as $k3 => $v3) {
								if (is_array($v3)) {
									foreach ($v3 as $k4 => $v4) {
										if (is_array($v4)) {
											foreach ($v4 as $k5 => $v5) {
												if (is_array($v5)) {
													foreach ($v5 as $k6 => $v6) {
														$_params["{$k1}[{$k2}][$k3][{$k4}][{$k5}][{$k6}]"] = $v6;
													}
												} else {
													$_params["{$k1}[{$k2}][$k3][{$k4}][{$k5}]"] = $v5;
												}
											}
										} else {
											$_params["{$k1}[{$k2}][$k3][{$k4}]"] = $v4;
										}
									}
								} else {
									$_params["{$k1}[{$k2}][$k3]"] = $v3;
								}
							}
						} else {
							$_params["{$k1}[{$k2}]"] = $v2;
						}
					}
				} else {
					$_params[$k1] = $v1;
				}
			}
			curl_setopt($curl, CURLOPT_POSTFIELDS, $_params);
		}

		$response = curl_exec($curl);

		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = explode("\r\n", substr($response, 0, $header_size));
		$body = substr($response, $header_size);

		$headers = array();
		foreach ($header as $hl) {
			if (strpos($hl, ":") === false)
				continue;
			$hl = trim($hl);
			$pieces = explode(": ", $hl);
			if (count($pieces) == 1) {
				$headers[$pieces[0]] = '';
			} else {
				if ($pieces[0] == 'MPXPP-Response-Detail') {
					$headers[$pieces[0]] = urldecode($pieces[1]);
				} else {
					$headers[$pieces[0]] = $pieces[1];
				}
			}
		}
		return array($headers, $body);
	}

	/**
	 * Gets a UTC-formatted timestamp
	 *
	 * @param	String	$offset	http://php.net/manual/en/datetime.formats.relative.php
	 * @return	String			Y-m-d H:i:s in UTC time
	 */
	public static function timestamp($offset = 'now') {
		$tz = new DateTimeZone("UTC");
		$dt = new DateTime($offset, $tz);
		return $dt->format("Y-m-d H:i:s");
	}


}


/*
 * Exceptions
 */
class ClientException extends \Exception { }
